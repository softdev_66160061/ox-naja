import java.util.Scanner;
//
public class OX {
    public static void main(String[] args) {
        System.out.println("!!! Welcome to XO Game !!!");
        Scanner input = new Scanner(System.in);
        int row ;
        int column ;
        int win = 0;
        String[][] table = new String[3][3];
        String[] turn = {"O","X","O","X","O","X","O","X","O"};
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = "-";
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
            }
        for(int r = 0; r < 9; r++){
            System.out.println("Turn " + turn[r]);
            System.out.print("Enter row number (1-3) :");
            row = input.nextInt();
            System.out.print("Enter column number (1-3) :");
            column = input.nextInt();
            row-=1;
            column-=1;
            table[row][column]=turn[r];
            System.out.println();
                
            for (int i = 0; i < 3; i++){
                for (int j = 0; j<3; j++){
                    System.out.print(table[i][j] + " ");
                }
                System.out.println();
            }
            for(int i = 0; i < 3;i++){
                if (table[i][0].equals("X") && table[i][1].equals("X") && table[i][2].equals("X") ||
                    table[i][0].equals("O") && table[i][1].equals("O") && table[i][2].equals("O")) {
                    win=1;
                }     
            }
            for(int i = 0; i < 3;i++){
                if (table[0][i].equals("X") && table[1][i].equals("X") && table[2][i].equals("X") ||
                    table[0][i].equals("O") && table[1][i].equals("O") && table[2][i].equals("O")) {
                    win=1;
                }
            }
            if (table[0][0].equals("X") && table[1][1].equals("X") && table[2][2].equals("X") ||
                table[0][0].equals("O") && table[1][1].equals("O") && table[2][2].equals("O") ||
                table[0][2].equals("X") && table[1][1].equals("X") && table[2][0].equals("X") ||
                table[0][2].equals("O") && table[1][1].equals("O") && table[2][0].equals("O")){
                win = 1;
            }
            if (win == 1){
                System.out.println(turn[r] + " Win!!");
                break;
            }
            if (r == 8 && win == 0) {
                System.out.println("draw!!!!");
            }
        }
    }
}
